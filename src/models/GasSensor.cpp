#include "GasSensor.h"

GasSensor::GasSensor(uint8_t analogPin, uint8_t digitalPin)
{
    this->analogPin = analogPin;
    this->digitalPin = digitalPin;

    pinMode(digitalPin, INPUT_PULLUP);
}

GasSensor::~GasSensor()
{
}

float GasSensor::analogValue() {
    int measurementSum = 0;
    
    for (uint8_t i = 0; i < SAMPLES_COUNT; i++) {
        measurementSum += analogRead(this->analogPin);
    }

    return measurementSum / SAMPLES_COUNT;
}

float GasSensor::value() {
    return this->analogValue();
}

float GasSensor::voltage() {
    int measurement = this->analogValue();

    return (measurement * 3.3) / 1024.0;
}

bool GasSensor::isGasDetected() {
    return !digitalRead(this->digitalPin);
}