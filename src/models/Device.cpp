#include "Device.h"

Device::Device(uint8_t pin)
{
    this->pin = pin;

    pinMode(pin, OUTPUT);
}

Device::~Device()
{
}

void Device::enable() {
    this->run = true;
    digitalWrite(this->pin, HIGH);
}

void Device::disable() {
    this->run = false;
    digitalWrite(this->pin, LOW);
}

bool Device::isEnabled() {
    return this->run;
}

