#ifndef __GasSensor_H__
#define __GasSensor_H__
#define SAMPLES_COUNT 5

#include <Arduino.h>

class GasSensor
{
	uint8_t analogPin;
	uint8_t digitalPin;

	float analogValue();

public:
	GasSensor(uint8_t analogPin, uint8_t digitalPin);
	~GasSensor();
    float value();
	float voltage();
	bool isGasDetected();
};

#endif //__GasSensor_H__
