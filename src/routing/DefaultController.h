#ifndef __DEFAULT_CONTROLLER_H__
#define __DEFAULT_CONTROLLER_H__

#ifndef Arduino_h
    #include <Arduino.h>
#endif
#include <DHT.h>
#include <ESPAsyncWebServer.h>
#include <ArduinoJson.h>
#include "consts.h"
#include "../models/GasSensor.h"

class DefaultController
{
    DHT* humiditySensor;
    GasSensor* mq2GasSensor;

public:
	DefaultController(DHT* humiditySensor, GasSensor* mq2GasSensor);
	~DefaultController();
    void mainRoute(AsyncWebServerRequest* request);
    void temperatureRoute(AsyncWebServerRequest* request, float temperature);
    void humidityRoute(AsyncWebServerRequest* request, float humidity);
    void gasRoute(AsyncWebServerRequest* request);
};

#endif