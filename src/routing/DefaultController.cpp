#include "DefaultController.h"

DefaultController::DefaultController(DHT* humiditySensor, GasSensor* mq2GasSensor)
{
    this->humiditySensor = humiditySensor;
    this->mq2GasSensor = mq2GasSensor;
}

DefaultController::~DefaultController()
{
}

void DefaultController::mainRoute(AsyncWebServerRequest* request)
{
    DynamicJsonDocument json(1024);
    json["name"] = "Gas Sensor";
    json["description"] = "";
    json["apiUrl"] = "/api/v1";
    String parsedJson;
    serializeJson(json, parsedJson);
    
    request->send(200, "application/json", parsedJson);

}

void DefaultController::temperatureRoute(AsyncWebServerRequest* request, float temperature)
{
    DynamicJsonDocument json(1024);
    json["value"] = String(temperature);
    json["unit"] = "°C";
    String parsedJson;
    serializeJson(json, parsedJson);

    request->send(200, "application/json", parsedJson);
}

void DefaultController::humidityRoute(AsyncWebServerRequest* request, float humidity)
{
    DynamicJsonDocument json(1024);
    json["value"] = String(humidity);
    json["unit"] = "%";
    String parsedJson;
    serializeJson(json, parsedJson);

    request->send(200, "application/json", parsedJson);
}

void DefaultController::gasRoute(AsyncWebServerRequest* request)
{
    DynamicJsonDocument json(1024);
    json["value"] = String(this->mq2GasSensor->value());
    json["voltage"] = String(this->mq2GasSensor->voltage());
    json["isGasDetected"] = String(this->mq2GasSensor->isGasDetected());
    String parsedJson;
    serializeJson(json, parsedJson);

    request->send(200, "application/json", parsedJson);
}